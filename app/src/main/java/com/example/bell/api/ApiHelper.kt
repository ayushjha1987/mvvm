package com.example.bell.api

import javax.inject.Inject

class ApiHelper @Inject constructor(private val apiService: ApiService) {

    suspend fun getYoutubePlaylist(token: String, part: String, maxResults: Int, key: String) =
        apiService.getYoutubePlaylist(token, part, maxResults, true, key)
}