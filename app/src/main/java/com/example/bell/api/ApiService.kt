package com.example.bell.api

import com.example.bell.model.YoutubeData
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {

    @Headers("application-type: REST", "Content-Type: application/json")
    @GET("playlists")
    suspend fun getYoutubePlaylist(@Header("Authorization") token:String, @Query("part") part: String, @Query("maxResults") maxResults: Int,@Query("mine") mine: Boolean, @Query("key") key: String
    ): YoutubeData

}