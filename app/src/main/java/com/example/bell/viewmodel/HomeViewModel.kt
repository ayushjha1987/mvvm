package com.example.bell.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bell.model.YoutubeData
import com.example.bell.repository.MainRepository
import com.example.bell.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect


class HomeViewModel @ViewModelInject constructor(private val mainRepository: MainRepository) :
    ViewModel(), LifecycleObserver {

    var youtubeData: MutableLiveData<Resource<YoutubeData>> = MutableLiveData()

    @ExperimentalCoroutinesApi
    suspend fun getYoutubePlaylist(
        token: String, part: String, maxResults: Int, key: String
    ): LiveData<Resource<YoutubeData>> {
        mainRepository.getYoutubePlaylist(token, part, maxResults, key).collect {
            youtubeData.value = it
        }
        return youtubeData
    }
}