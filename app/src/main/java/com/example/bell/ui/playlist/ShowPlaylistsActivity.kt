package com.example.bell.ui.ui.playlist

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bell.R
import com.example.bell.adapter.YoutubeDataAdapter
import com.example.bell.model.YoutubeData
import com.example.bell.utils.Resource
import com.example.bell.utils.Status
import com.example.bell.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch


@AndroidEntryPoint
class ShowPlaylistsActivity : AppCompatActivity() {

    private val homeViewModel: HomeViewModel by viewModels()
    private lateinit var adapterYoutube: YoutubeDataAdapter

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_home)
        setupUI()
        lifecycleScope.launch {
            homeViewModel.getYoutubePlaylist(
                "Bearer " + intent.getStringExtra("TOKEN"),
                "snippet,contentDetails",
                25,
                getString(R.string.google_api_key)
            )
        }
        setupObservers()
    }

    private fun setupUI() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapterYoutube = YoutubeDataAdapter()
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                (recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerView.adapter = adapterYoutube
    }

    private fun setupObservers() {
        val observer = Observer<Resource<YoutubeData>> {
            it.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        recyclerView.visibility = View.VISIBLE
                        resource.data?.let { repos ->
                            retrieveList(repos)
                        }
                    }
                    Status.ERROR -> {
                        recyclerView.visibility = View.GONE
                    }
                    Status.LOADING -> {
                        recyclerView.visibility = View.GONE
                    }
                }
            }
        }
        homeViewModel.youtubeData.observe(this, observer)
    }

    private fun retrieveList(repos: YoutubeData) {
        adapterYoutube.apply {
            getYoutubeData(repos)
            notifyDataSetChanged()
        }
    }
}
