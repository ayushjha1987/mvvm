package com.example.bell.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}