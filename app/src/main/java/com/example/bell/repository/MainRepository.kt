package com.example.bell.repository

import com.example.bell.api.ApiHelper
import com.example.bell.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiHelper: ApiHelper) {

    @ExperimentalCoroutinesApi
    fun getYoutubePlaylist(token: String, part: String, maxResults: Int, key: String) = flow {
        emit(Resource.loading(null))
        val response = apiHelper.getYoutubePlaylist(token, part, maxResults, key)
        if (response != null) {
            emit(Resource.success(response))
        } else {
            emit(Resource.error(null, "Http Error!"))
        }
    }

    

}