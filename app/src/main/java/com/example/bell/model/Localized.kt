package com.example.bell.model

data class Localized (

	val title : String,
	val description : String
)