package com.example.bell.model

data class Maxres (

	val url : String,
	val width : Int,
	val height : Int
)