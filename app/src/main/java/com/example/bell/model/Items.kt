package com.example.bell.model


data class Items (

    val kind : String,
    val etag : String,
    val id : String,
    val snippet : Snippet,
    val contentDetails : ContentDetails,
    val player : Player
)