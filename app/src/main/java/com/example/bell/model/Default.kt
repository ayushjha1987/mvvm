
package com.example.bell.model
data class Default (

	val url : String,
	val width : Int,
	val height : Int
)