package com.example.bell.model

data class Medium (

	val url : String,
	val width : Int,
	val height : Int
)