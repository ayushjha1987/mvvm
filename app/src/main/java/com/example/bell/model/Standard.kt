package com.example.bell.model

data class Standard (

	val url : String,
	val width : Int,
	val height : Int
)