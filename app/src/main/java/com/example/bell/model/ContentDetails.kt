package com.example.bell.model

data class ContentDetails(

	val itemCount: Int
)