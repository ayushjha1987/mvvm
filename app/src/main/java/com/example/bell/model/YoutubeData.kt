package com.example.bell.model

data class YoutubeData (

	val kind : String,
	val etag : String,
	val pageInfo : PageInfo,
	val items : List<Items>
)