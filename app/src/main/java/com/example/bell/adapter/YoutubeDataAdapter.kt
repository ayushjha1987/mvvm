package com.example.bell.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bell.R
import com.example.bell.model.YoutubeData


class YoutubeDataAdapter : RecyclerView.Adapter<YoutubeDataAdapter.ViewHolder>() {
    private var youtubeData: YoutubeData? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_layout, viewGroup, false)
        return ViewHolder(view)
    }

    fun getYoutubeData(youtubeData: YoutubeData) {
        this.youtubeData = youtubeData
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val snippet = youtubeData?.items?.get(i)?.snippet
        viewHolder.playlistTitle.text = snippet?.title ?: ""
        viewHolder.number.text = viewHolder.itemView.context.getString(R.string.videos_number_title)+youtubeData?.items?.size.toString()
        Glide.with(viewHolder.imageView).load(snippet?.thumbnails?.maxres?.url).into(viewHolder.imageView)
    }

    override fun getItemCount(): Int {
        return youtubeData?.items?.size?:0
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById<View>(R.id.imageView) as ImageView
        val playlistTitle: TextView = view.findViewById<View>(R.id.playlistTitle) as TextView
        val number: TextView = view.findViewById<View>(R.id.number) as TextView
    }

}